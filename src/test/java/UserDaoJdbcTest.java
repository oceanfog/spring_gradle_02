import domain.Level;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.User;
import service.UserDao;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Dell on 2016-04-30.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class UserDaoJdbcTest {

    @Autowired
    private UserDao userDao;

    private User user1, user2, user3;

    @Before
    public void setUp() throws Exception {
        this.user1 = new User("oceanfog", "kyeongork", "1123", Level.BASIC, 1, 0, "oceanfog@gmail.com");
        this.user2 = new User("oceanfog2", "kyeongork", "1123", Level.SILVER, 55, 10, "oceanfog2@gmail.com");
        this.user2 = new User("oceanfog3", "kyeongork", "1123", Level.GOLD, 100, 40, "oceanfog3@gmail.com");

    }

    @Test
    public void addAndGetTest() throws Exception {

        userDao.deleteAll();
        assertEquals(0, userDao.getCount());
        userDao.add(user1);

        User selectedUser = userDao.get(user1.getId());
        assertEquals(1, userDao.getCount());


        List<User> list = userDao.getAll();
        User selectAllUser = list.get(0);
        assertEquals("oceanfog",selectAllUser.getId());
        assertEquals("oceanfog@gmail.com",selectAllUser.getEmail());

    }

    @Test
    public void update() throws Exception {

        userDao.deleteAll();
        assertEquals(0, userDao.getCount());
        userDao.add(user1);

        user1.setName("hello");
        userDao.update(user1);

        User selectedUser = userDao.get(user1.getId());

        assertEquals("hello", selectedUser.getName());

    }

    @Test
    public void getAll() throws Exception {

        userDao.deleteAll();

        List<User> list = userDao.getAll();

        assertEquals(0, list.size());

    }

    @Test
    public void timestamp() throws Exception {

        long timestamp = (System.currentTimeMillis() / 1000L) * 1000L;
        //String changeLogUrl = liveScoreChangeUrl + "?" + timestamp;

        System.out.println(timestamp);

    }
}