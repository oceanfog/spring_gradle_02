import domain.Level;
import org.junit.Before;
import org.junit.Test;
import service.User;

/**
 * Created by Dell on 2016-05-09.
 */
public class UserTest {

    User user;

    @Before
    public void setUp() throws Exception {
        user = new User();

    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotUpgradeLevel() throws Exception {

        Level[] levels = Level.values();
        for(Level level: levels){
            if(level.nextLevel() != null) continue;
            user.setLevel(level);
            user.upgradeLevel();

        }

    }

}