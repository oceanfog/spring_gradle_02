import domain.Level;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.User;
import service.UserDao;
import service.UserService;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import static service.UserService.MIN_LOGCOUNT_FOR_SILVER;
import static service.UserService.MIN_RECOMMEND_FOR_GOLD;

/**
 * Created by Dell on 2016-05-09.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class UserServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserDao userDao;

    @Autowired
    private DataSource dataSource;

    List<User> users;

    @Before
    public void setUp() throws Exception {
        users = new ArrayList<>();
        users.add(new User("yeajin", "yeajin", "p1", Level.BASIC, MIN_LOGCOUNT_FOR_SILVER-1, 0, "yeajin@gmail.com"));
        users.add(new User("jieun", "jieun", "p2", Level.BASIC, MIN_LOGCOUNT_FOR_SILVER, 0,"jieun@gmail.com"));
        users.add(new User("seoyeong", "seoyeong", "p3", Level.SILVER, 60, MIN_RECOMMEND_FOR_GOLD-1,"seoyeong@gmail.com"));
        users.add(new User("jisu", "jisu", "p4", Level.SILVER, 60, MIN_RECOMMEND_FOR_GOLD,"jisu@gmail.com"));
        users.add(new User("boram", "boram", "p5", Level.GOLD, 100, 100,"boram@gmail.com"));

    }

    @Test
    public void isNotNull() throws Exception {

        assertNotNull(userService);
    }

    @Test
    public void updateLevel() throws Exception {

        userDao.deleteAll();

        assertEquals(0, userDao.getCount());

        for(User user: users){
            userDao.add(user);
        }

        userService.updateLevels();

        checkLevelUpgraded(users.get(0), false);
        checkLevelUpgraded(users.get(1), true);
        checkLevelUpgraded(users.get(2), false);
        checkLevelUpgraded(users.get(3), true);
        checkLevelUpgraded(users.get(4), false);

    }

    @Test
    public void add() throws Exception {
        userDao.deleteAll();

        User user1 = users.get(0);
        User user4 = users.get(4);
        user1.setLevel(null);

        userService.add(user1);
        userService.add(user4);

        User addUser = userDao.get(user1.getId());
        User addUser4 = userDao.get(user4.getId());

        assertEquals(Level.BASIC, addUser.getLevel());
        assertEquals(Level.GOLD, addUser4.getLevel());

    }



    private void checkLevelUpgraded(User user, Boolean upgraded){
        User updateUser = userDao.get(user.getId());
        //user하고 updateUser하고 비교하면 될 듯
        if(upgraded){
            //업그레이드가 됐는지 확인
            assertEquals(user.getLevel().nextLevel(), updateUser.getLevel());
        }else{
            //업그레이드가 안됐는지 확인
            assertEquals(user.getLevel(), updateUser.getLevel());
        }
    }


    @Test
    public void upgradeAllOrNothing() throws Exception {

        UserService testUserService = new TestUserService(users.get(3).getId());
        testUserService.setUserDao(this.userDao);
        testUserService.setDataSource(this.dataSource);
        userDao.deleteAll();

        for(User user : users) userDao.add(user);


            try {
                testUserService.updateLevels();
                fail("test exception expected");
            } catch (TestUserServiceException e) {
                System.out.println("error");
            }
            checkLevelUpgraded(users.get(1), false);
    }
}

class TestUserService extends UserService {
    private String id;

    protected TestUserService(String id) {
        this.id = id;
    }

    protected void upgradeLevel(User user){
        if(user.getId().equals(this.id) ) throw new TestUserServiceException();
        super.upgradeLevel(user);
    }

}

class TestUserServiceException extends RuntimeException{

}