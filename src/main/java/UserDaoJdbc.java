import domain.Level;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import service.User;
import service.UserDao;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Dell on 2016-04-30.
 */
public class UserDaoJdbc implements UserDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    private RowMapper<User> userMapper =
        new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                User user = new User();
                user.setId(rs.getString("id"));
                user.setName(rs.getString("name"));
                user.setPassword(rs.getString("password"));
                user.setLevel(Level.valueOf(rs.getInt("level")));
                user.setLogin(rs.getInt("login"));
                user.setRecommend(rs.getInt("recommend"));
                user.setEmail(rs.getString("email"));
                return user;
            }
        };

    public void add(User user){
        jdbcTemplate.update("insert into users (id, name, password, level, login, recommend, email) values(?, ?, ?, ?, ?, ?, ?)"
        ,user.getId()
        ,user.getName()
        ,user.getPassword()
        ,user.getLevel().intValue()
        ,user.getLogin()
        ,user.getRecommend()
        ,user.getEmail()

        );
    }

    public User get(String id){
        return this.jdbcTemplate.queryForObject("select * from users where id=?"
            , new Object[]{id}
            , userMapper);
    }

    public void deleteAll(){
        this.jdbcTemplate.update("delete from users");
    }


    public int getCount(){
        return this.jdbcTemplate.queryForObject("select count(*) from users"
            , Integer.class
        );
    }

    public List<User> getAll(){
        return this.jdbcTemplate.query("select * from users"
                , userMapper);

    }

    public void update(User user){
        jdbcTemplate.update("update users set name=?, password=?, level=?, login=?, recommend=? , email=? where id=?"
                ,user.getName()
                ,user.getPassword()
                ,user.getLevel().intValue()
                ,user.getLogin()
                ,user.getRecommend()
                ,user.getEmail()
                ,user.getId()

        );

    }

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
}
