package service;

import java.util.List;

/**
 * Created by Dell on 2016-05-09.
 */
public interface UserDao {
    public void add(User user);
    public User get(String id);
    public void deleteAll();
    public int getCount();
    public List<User> getAll();
    public void update(User user);
}
