package service;

/**
 * Created by Dell on 2016-05-10.
 */
public interface UserLevelUpgradePolicy {
    Boolean canUpgradeLevel(User user);
    void upgradeLevel(User user);
}
